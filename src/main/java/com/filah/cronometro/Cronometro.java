package com.filah.cronometro;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Rodrigo Romero
 * @version 1.0
 * @since 09/10/2014
 */
public class Cronometro
{

    private Date dataInicial;
    private Date dataFinal;
    private String dataInicialStr;
    private String dataFinalStr;
    private Long tempoInicial;
    private Long tempoFinal;
    private Integer horaInicial;
    private Integer horaFinal;
    private Integer minutoInical;
    private Integer minutoFinal;
    private String tempoTotal;
    private final Locale locale;
    private Boolean iniciado = false;
    private Boolean parado = false;
    DateFormat format;

    public Cronometro()
    {
        locale = new Locale("pt", "BR");
        format = DateFormat.getDateInstance(DateFormat.FULL, locale);
    }

    public Cronometro(Locale locale)
    {
        this.locale = locale;
        format = DateFormat.getDateInstance(DateFormat.FULL, this.locale);
    }

    /**
     * Inicia o cronometro
     */
    public void iniciar()
    {

        if (iniciado)
        {
            System.err.print("O cronometro já foi iniciado!");
            return;
        }

        iniciado = true;
        parado = false;
        tempoInicial = System.currentTimeMillis();
        dataInicial = new Date();
        dataInicialStr = format.format(dataInicial);

        System.out.println("Iniciando cronometro: " + dataInicialStr);
    }

    /**
     * Para o cronometro
     */
    public void parar()
    {
        if (iniciado)
        {
            tempoFinal = System.currentTimeMillis();

            dataFinal = new Date();
            dataFinalStr = format.format(dataFinal);

            System.out.println("Finalizando o cronometro: " + dataFinalStr);
            iniciado = false;
            parado = true;
        }
        else
        {
            System.err.println("O crometro não foi iniciado, e não pode ser parado");
        }
    }

    /**
     * Exibe o tempo total registrado sem milesegundos
     *
     * @return tempo Total
     */
    public String getTempoTotal()
    {
        if (parado)
            return calcularTempo(false);
        else
            return "";
    }

    /**
     * Exibe o tempo total registrado com milesegundos
     *
     * @return tempo total
     */
    public String getTempoTotalComMilesegundos()
    {
        if (parado)
            return calcularTempo(true);
        else
            return "";
    }

    /**
     * Exibe tempo total sem milesegundos
     */
    public void exibirTotal()
    {
        if (parado)
            System.out.println("Tempo Total: " + calcularTempo(false));
        else
            System.err.println("O cronometro não foi parado.");
    }

    /**
     * Exibe tempo total com milesegundos
     */
    public void exibirTotalComMilesegundos()
    {
        if(parado)
            System.out.println("Tempo Total: " + calcularTempo(true));
        else
            System.err.println("O cronometro não foi parado.");
    }

    private String calcularTempo(Boolean milesegundos)
    {
        Long total = tempoFinal - tempoInicial;

        long hours = total / 1000 / 60 / 60;
        total -= hours * 1000 * 60 * 60;

        long minutes = total / 1000 / 60;
        total -= minutes * 1000 * 60;

        long seconds = total / 1000;
        total -= seconds * 1000;

        StringBuilder time = new StringBuilder();

        if (hours > 0)
            time.append(hours + ":");
        else
        {
            time.append("00:");
        }

        if (hours > 0 && minutes < 10)
            time.append("0");
        if (minutes == 0)
            time.append("0");

        time.append(minutes + ":");

        if (seconds < 10)
            time.append("0");

        time.append(seconds);

        if (milesegundos)
        {
            time.append(".");
            if (total < 100)
                time.append("0");
            if (total < 10)
                time.append("0");
            time.append(total);
        }

        return time.toString();
    }

}
